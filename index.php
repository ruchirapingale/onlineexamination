<?php

defined('BASE_PATH')
    || define('BASE_PATH', realpath(dirname(__FILE__)));
	
// Define path to application directory
defined('APPLICATION_PATH')
    || define('APPLICATION_PATH', BASE_PATH . '/application');	
	
// Define application environment
defined('APPLICATION_ENV')
    || define('APPLICATION_ENV', (getenv('APPLICATION_ENV') ? getenv('APPLICATION_ENV') : 'production'));
	
// Define Configuration Directory
defined('OE_CONFIGURATION_DIR')
    || define('OE_CONFIGURATION_DIR', APPLICATION_PATH . '/configs');
	
// Define configuration file
defined('OE_CONFIGURATION_SYSTEM')
    || define('OE_CONFIGURATION_SYSTEM', OE_CONFIGURATION_DIR . '/configs/system.xml');
	

// Ensure library/ is on include_path
set_include_path(implode(PATH_SEPARATOR, array(
    realpath(dirname(__FILE__) . '/library'),
    get_include_path(),
)));

//Autoloader configuration
require_once "Zend/Loader/Autoloader.php";
$autoloader = Zend_Loader_Autoloader::getInstance();
$autoloader->suppressNotFoundWarnings(false);
$autoloader->setFallbackAutoloader(false);
$autoloaderClassMapFiles = array(
    APPLICATION_PATH . "/configs/autoload-classmap.php",
);

foreach ($autoloaderClassMapFiles as $autoloaderClassMapFile) {
    if(file_exists($autoloaderClassMapFile)) {
        $classMapAutoLoader = new Zend_Loader_ClassMapAutoloader(array($autoloaderClassMapFile));
        $classMapAutoLoader->register();
    }
}

/** Zend_Application */
require_once 'Zend/Application.php';

OE_Install::run();	

// Create application, bootstrap, and run
$application = new Zend_Application(
    APPLICATION_ENV,
    APPLICATION_PATH . '/configs/application.ini'
);
$application->bootstrap()
            ->run();
		