<?php

class Users_Form_Login extends Zend_Form {

  public function init() {
    /* Form Elements & Other Definitions Here ... */
    // initialize form
    $this->setAction('Users/login/authenticate')
        ->setMethod('POST');

// create text input for name
    $username = new Zend_Form_Element_Text('username');
    $username->setLabel('Username')
        ->removeDecorator('HtmlTag')
        ->setOptions(array('size' => '15', 'class' => 'form-text required'))
        ->setRequired(true)
        ->addValidator('Alnum')
        ->addFilter('HtmlEntities')
        ->addFilter('StringTrim');

// create text input for password
    $password = new Zend_Form_Element_Password('password');
    $password->setLabel('Password')
        ->removeDecorator('HtmlTag')
        ->setOptions(array('size' => '15', 'class' => 'form-text required'))
        ->setRequired(true)
        ->addFilter('HtmlEntities')
        ->addFilter('StringTrim');

// create submit button
    $submit = new Zend_Form_Element_Submit('submit');
    $submit->setLabel('Log In')
        ->setOptions(array('class' => 'submit form-submit'))->setDecorators(array('ViewHelper'));

// attach elements to form
    $this->addElement($username)
        ->addElement($password)
        ->addElement($submit);
  }

}

